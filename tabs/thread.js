/* display image on modal on request*/

function closeModal() {
    var modal = document.getElementById("threadModal");
    modal.style.display = "none";
}

function initModal() {
    var pvs = document.getElementsByClassName("preview");
    
    for (var i = 0; i < pvs.length; i++) {
        var pv = pvs[i];
        if (pv.tagName.toLowerCase()=="img") {
            pv.onclick = function () { 
                var modal = document.getElementById("threadModal");
                var modalImg = document.getElementById("modalImage");
        
                modal.style.display = "block";
                modalImg.src = this.src; 
            }
        }
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    if (span) {
        span.onclick = closeModal; 
    }
}


/* check whether thread-content is changed and reinitialize modal handling */
function init() {
    const targetNode = document.getElementById("thread-content");
    const config = { childList: true };

    const observer = new MutationObserver(function(mutations) {
        initModal();
    });

    observer.observe(targetNode, config);
}

document.addEventListener("DOMContentLoaded", init);