// sanitizes the html from the mastodon api
// shouldn't be really necessary as this is also displayed, when clicking on the link,
// but just in case
function addSanitizedHTML(parentElement, html) {
    // Create a new DOM element to parse the HTML code
    let doc = new DOMParser().parseFromString(html, 'text/html');
    var tempEl = doc.body;
    
    // Loop over children 
    for (var i = 0; i < tempEl.childNodes.length; i++) {
      var node = tempEl.childNodes[i];
      let el = null;
      if (node.nodeType === 1) { // Element node
        // Filter out potentially dangerous elements
        if (["script", "style", "iframe", "object", "embed"].indexOf(node.tagName.toLowerCase()) !== -1) {
          continue;
        }
        // ok, open tag
        el = document.createElement(node.tagName.toLowerCase());
        // Filter out potentially dangerous attributes
        for (var j = 0; j < node.attributes.length; j++) {
          var attrName = node.attributes[j].name.toLowerCase();
          var attrValue = node.attributes[j].value;
          if (["onload", "onerror", "onclick", "onmouseover", "onmouseout", "onkeyup"].indexOf(attrName) !== -1) {
            continue;
          }
          if (attrName === "href" && !/^https?:\/\//i.test(attrValue)) {
            continue;
          }
          // Allow all other attributes
          el.setAttribute(attrName, attrValue);
        }
        // Recursively filter child nodes
        addSanitizedHTML(el, node.innerHTML);
      } else if (node.nodeType === 3) { // Text node
        el = document.createTextNode(node.nodeValue);
      }
      if (el) {
        parentElement.appendChild(el);
      }
    }
  }
  
// get text from html using DOMParser
function htmlToPlainText(html) {
    let doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || "";
}

// escape html special characters
function escapeSpecial(htmlStr) {
    return htmlStr.replace(/&/g, "&amp;")
          .replace(/</g, "&lt;")
          .replace(/>/g, "&gt;")
          .replace(/"/g, "&quot;")
          .replace(/'/g, "&#39;");        
 
 }
