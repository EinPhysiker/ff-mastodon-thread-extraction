// check if link has the standard format of a mastodon toot link
function isMastodonTootLink(url) {
    let tootRegex = /^https?:\/\/([^\/]+)\/@([^\/]+)\/(\d+)/;
    let match = url.match(tootRegex);
    if (match && match.length === 4) {
        let [fullUrl, hostname, username, tootId] = match;
        if (username && tootId) {
            return { hostname, tootId };
        }
    }
    return null;
}

// get the info got a single toot using hostname and tootId
async function getTootInfo(hostname, tootId) {
    let apiEndpoint = `https://${hostname}/api/v1/statuses/${tootId}`;
    return fetch(apiEndpoint)
        .then(response => response.json())
        .then(data => {
            console.log("Toot info:", data);
            return data;
        })
        .catch(error => {
            console.log("Failed to get toot info:", error);
        });
}

// get the info for the full thread
// we use the hostname of the last thread and read all toots until there is
// no parent toot id
async function getTootThread(hostname, tootId) {
    thread = [];
    while (tootId) {
        let tootData = await getTootInfo(hostname, tootId);
        thread.push(tootData);
        tootId = tootData.in_reply_to_id
    }
    return thread;
}
