// listen to button events from the menu
document.addEventListener("click", doAction);


// write the thead to a string (markdown)
async function writeThreadMarkdown(thread) {
    let output = '';
    let author_old = '';
    // walk through thread from top toot
    for (let i = thread.length - 1; i >= 0; i--) {
        const toot = thread[i];
        const author = `${toot.account.display_name} (@${toot.account.username})`;
        // give author only if changed
        if (author!== author_old) {
            output += `### ${author}\n\n`;
            author_old = author;
        }
        // provide toot content 
        output += htmlToPlainText(toot.content) + "\n\n";
        // add any media
        if (toot.media_attachments) {
            for (const media of toot.media_attachments) {
                output += `![${media.id}](${media.preview_url})\n\n`;
                if (media.description) {
                    output += "Description: " + htmlToPlainText(media.description) + "\n\n";
                }
                output += `[Link to ${media.type}](${media.url})\n\n`;
            }
        }
    }
    output += '';
    return output;
}

// write the thead to a string (html)
async function writeThreadHtml(thread) {
    let output = '';
    let author_old = '';
    let author_start = '';
    // walk through thread from top toot
    for (let i = thread.length - 1; i >= 0; i--) {
        const toot = thread[i];
        var date = new Date(toot.created_at);
        var dateString = date.toUTCString();
        const author = `${toot.account.display_name} (<a href="${toot.account.url}">@${toot.account.username}</a>)`;
        if (author_start === '') {
            author_start = author;            
        }
        var use_class = 'author';  
        var toot_class = 'toot left';   
        if (author != author_start) {
            use_class = 'reply';
            toot_class = 'toot right';
        }            
        // give author only if changed
        if (author!== author_old) {
            output += `<div class="header ${use_class}"><h3>${author}</h3><div class="date">Created at: ${dateString}</div></div>\n\n`;
            author_old = author;
        } 
        // provide toot content
        output += `<div class="${toot_class}">` + toot.content; 
        // add any media (tested only for images and gifs, so far didn't find video or audio attachments)
        if (toot.media_attachments) {
            for (let j = toot.media_attachments.length - 1; j >= 0; j--) {
                const media = toot.media_attachments[j];
                var title = '';
                if (media.description) {
                    title = escapeSpecial(media.description);
                } 
                if (media.type === 'image') {
                    output += `<img title="${title}" src="${media.url}" class="preview"></img>\n\n`;
                } else if (media.type === 'video') { 
                    output += `<video class="preview" title="${title}" controls><source src="${media.url}" type="video/mp4"></video>\n\n`;
                } else if (media.type === 'gifv') { 
                    output += `<video class="preview" title="${title}" autoplay loop><source src="${media.url}" type="video/mp4"></video>\n\n`;
                } else if (media.type === 'audio') {
                    output += `<audio class="preview" title="${title}" controls><source src="${media.url}" type="audio/mpeg"></audio>\n\n`;
                }
            }
            output += '<div id="threadModal" class="modal"><span class="close">&times;</span><img class="modal-content" id="modalImage"></div>';
        }
        output += `<div class="toot-link"><a href="${toot.url}">Link</a></div>`;
        output += '</div>\n\n';
    }
    return output;
}

// copy the output to the clipboard
function copyTextToClipboard(text) {
    navigator.clipboard.writeText(text).then(function() {
        document.getElementById('status').innerText = 'Copied to clipboard!';
    }, function() {
        document.getElementById('status').innerText = 'Copying failed';
    });
}

// load the tab
function showInWindow(output) {
    // open a blank "target" window
    var threadWindow = window.open('', 'MastodonThreadDisplay', '');

    // if the "target" window was just opened, change its url
    if(threadWindow.location.href === 'about:blank'){
        threadWindow.location.href = '/tabs/thread.html';
        threadWindow.onload = function() { console.log("onload"); setThreadElement(threadWindow, output); }
    } else {
        setThreadElement(threadWindow, output);
    }
    threadWindow.focus();
    return;
}

function setThreadElement(window, output) {
    if (window && !window.closed && output.length > 0) {
        const threadContentEl = window.document.getElementById('thread-content');
        if (threadContentEl) {
            // clean up the old thread
            while (threadContentEl.firstChild) {
                threadContentEl.firstChild.remove()
            }
            // add the new thread
            addSanitizedHTML(threadContentEl, output);
            return true;
        }   
    }
    return false;    
}

async function doAction(e) {
    console.log("doAction:", e.target.id);
    const statusEl = document.getElementById('status');

    // get options from local storage (dummy for now)
    let options = await browser.storage.local.get();
    console.log("options:", options);

    navigator.clipboard.readText().then(async function(clipboardText) {
        if (clipboardText && clipboardText.startsWith("http")) {
        let mastodonLink = isMastodonTootLink(clipboardText);
            if (mastodonLink) {
                console.log("Mastodon toot link:", mastodonLink.hostname, mastodonLink.tootId);
                let thread = await getTootThread(mastodonLink.hostname, mastodonLink.tootId);
                console.log("Thread data:", thread); 
                let output = "";
                if (e.target.id.startsWith("md")) {
                    output = await writeThreadMarkdown(thread);
                    if (e.target.id.endsWith("tab")) 
                    output = `<pre>${output}</pre>`;
                } else if (e.target.id.startsWith("html")) {
                    output = await writeThreadHtml(thread);
                } else {
                    statusEl.innerText = "Unknown output format";
                }
                if (output != "") {
                    if (e.target.id.endsWith("cb")) {
                        copyTextToClipboard(output);
                    } else if (e.target.id.endsWith("tab")) {                
                        showInWindow(output);
                    }
                }
            } else {
                statusEl.innerText = "Not a mastodon toot link";
            }
        } else {
            statusEl.innerText = "No link URL found in clipboard";
        }
    }, function() {
        statusEl.innerText = "Failed to read clipboard";
    });
}

