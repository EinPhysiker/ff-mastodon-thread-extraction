function saveOptions(e) {
    browser.storage.local.set({
      server: document.querySelector("#server").value,
      apikey: document.querySelector("#apikey").value
    });
    e.preventDefault();
  }

function getOptions() {
  var option = browser.storage.local.get();
  option.then(function(result) {
    console.log(result);
    if (result.server) {
      document.querySelector("#server").value = result.server;
    }
    if (result.apikey) {
      document.querySelector("#apikey").value = result.apikey;
    }
  });
}

document.querySelector("form").addEventListener("submit", saveOptions);

window.addEventListener("load", getOptions);