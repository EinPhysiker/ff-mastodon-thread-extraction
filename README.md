# FF Mastodon Thread Extraction

Disclaimer: This is a very first try on a Firefox extension and also very first steps on javascript. Thus, I have to thank the internet community for the vast amount of examples on the various steps. 

Release: v0.6-branch
Development: main 

## Getting started

For testing the newest git version, copy/clone the files to your computer. Then open "about:debugging" in Firefox, select "This Firefox" and load it as temporary extension. You need to select "manifest.json".

Releases can be downloaded from https://gitlab.com/EinPhysiker/ff-mastodon-thread-extraction/-/releases

## What it does

The extension creates a button in the icon bar, which opens a menu with options "Markdown" and "HTML", either for opening in a new tab ("Window") or for copying to the clipboard. If you copy a link to a mastodon toot (i.e. of the form https://hostname/@user/id) to the clipboard and click on the menu buttons, the script interprets the linked toot as the last one in a thread and reconstructs the thread until it reaches the top most parent. The result is copied to the clipboard or displayed in a tab. The tab is reused for further links. 

Thus, if one wants to use it, one can use the "Copy link" command in the stardard Mastodon or any on any other page.

Media are included as markdown links with the included description. Thus, you need to copy this to a markdown viewer in order to see the images. For HTML preview images are shown. A click on the preview images opens the image on a layer on top of the page. The description is added as tooltip on the preview. 

## To do

A lot: 
* code improvement
* toot formatting
* configuration options
* output in a file
* ...
